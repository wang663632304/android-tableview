package co.bytemark.android.opentools.tableview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.StateListDrawable;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import co.bytemark.android.opentools.autoresizer.AutoResizer;

/**
 * <b>Description:</b>
 * <br>The TableViewCell class defines the attributes and behavior of the cells that appear in a TableView object.
 * <br>
 * <br>A TableViewCell object (or table cell) includes properties and methods for managing cell selection, highlighted state, editing state and controls, accessory views, and cell background. The class additionally includes properties for setting and managing cell content, specifically text and images.
 * <br>
 * <br>You have two ways of extending the standard TableViewCell object beyond the given styles. To create cells with multiple, variously formatted and sized strings and images for content, you can get the cells content view (through its <code>m_ContentView</code> property) and add subviews to it. You can also subclass TableViewCell to obtain cell characteristics and behavior specific to your application's needs.
 * @author kevin_rejko
 */


public class TableViewCell extends LinearLayout 
{
	public Context m_cContext;
	protected TableView tableView;
	protected IndexPath indexPath;

	private LinearLayout m_llBackgroundView;
	private LinearLayout m_llContentView;
	private ImageView m_ivImageView;
	private TextView m_tvTextView;
	private TextView m_tvDetailTextView;
	private ImageView m_ivAccessoryImage;
	
	protected final int m_nZERO = 0;
	protected final int m_nMinHeight = AutoResizer.resizeRelativeToOriginalWidth(50);
	protected final int m_nCellPadding = AutoResizer.resizeRelativeToOriginalWidth(5);
	protected final int m_nAccessoryPaddingTopBottom = m_nZERO;
	protected final int m_nAccessoryPaddingLeftRight = m_nZERO;
	protected final int m_nAccessoryPaddingRight = AutoResizer.resizeRelativeToOriginalWidth(8);
	protected final int m_nTextContainerPaddingLeftRight = AutoResizer.resizeRelativeToOriginalWidth(8);
	protected final int m_nTextViewTextSize = AutoResizer.resizeRelativeToOriginalWidth(16);
	protected final int m_nDetailTextViewTextSize = AutoResizer.resizeRelativeToOriginalWidth(12);
	protected int m_cslTextColor = Color.BLACK;
	private int m_cslDetailTextColor = Color.DKGRAY;
	protected float m_fCORNER_RADIUS = AutoResizer.resizeRelativeToOriginalWidth(10);
	
	
	public static enum TableViewCellSelectionStyle  {None, Blue, Gray, Custom}
	protected TableViewCellSelectionStyle m_cssSelectionStyle = TableViewCellSelectionStyle.Blue;
	private View selectedView;
	
	protected static enum SelectionStatus  {Selected, Unselected}
	protected SelectionStatus m_ssSelectionStatus = SelectionStatus.Unselected;
	
	
	private int[] m_arrnBackgroundColor = new int[] {Color.WHITE, Color.WHITE};
	protected int m_nBorderColor = Color.parseColor("#FFAAAAAA");
	protected int m_nBlueStyleColor = Color.parseColor("#AF2570BA");
	protected int m_nGreyStyleColor = Color.parseColor("#AF9CA4AA");
	private int m_arrnCustomCellSelectionStyle;

	
	
	
	
	
	/**
	 * @category Constructors
	 */
	
	public TableViewCell (Context context)
	{
		super (context);
		throw new RuntimeException ("TableViewCell should not be instantiated via context only constructor");
	}
	
	public TableViewCell(Context context, TableView tableView, IndexPath indexPath) {
		this (context, tableView, indexPath, null);
	}
	
	public TableViewCell(Context context, TableView tableView, IndexPath indexPath, String xmlName) 
	{
		this (context, tableView, indexPath, ((xmlName!=null)?context.getResources().getIdentifier(xmlName, "layout",context.getPackageName()):0));		
	}
	
	public TableViewCell(Context context, final TableView tableView, IndexPath indexPath,  int xmlID) 
	{
		super(context);
		this.m_cContext = context;
		this.tableView = tableView;
		this.indexPath = indexPath;
		
		if(xmlID!=0){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (inflater != null) {
				inflater.inflate(xmlID, this);
			}
			
		}
		
		this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	
            }
        });
		
		this.setOnTouchListener(new OnTouchListener() {

	        public boolean onTouch(View v, MotionEvent event) {
	        	selectedView = v;
	        	
	            switch (event.getAction()) {
	                case MotionEvent.ACTION_DOWN: {
	                	if (tableView.cellShouldSelect()){
	                		displayOppositeSelection();
	                	}
	                    break;
	                }
	                case MotionEvent.ACTION_UP: {
	                	cellTouched ();
	                	
	                    break;
	                }
	                case MotionEvent.ACTION_MOVE: {
	                	displaySelectedForCurrentStatus();
	                	break;
	                }
	                case MotionEvent.ACTION_CANCEL:{
	                	displaySelectedForCurrentStatus();
	                	break;
	                }
	                
	            }
	            return false;
	        }
	    });

	}
	
	
	/**
	 * @category Interface_Layout
	 */
	
	private LinearLayout newBackgroundView (Context _cContext)
	{
		LinearLayout _llBackgroundView = new LinearLayout (_cContext);
		AbsListView.LayoutParams  _lpContentViewParams = new AbsListView.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		_llBackgroundView.setLayoutParams(_lpContentViewParams);
		_llBackgroundView.setMinimumHeight(m_nMinHeight);
		_llBackgroundView.setPadding(m_nCellPadding, m_nCellPadding, m_nCellPadding, m_nCellPadding);
		_llBackgroundView.setOrientation(HORIZONTAL);
		_llBackgroundView.setGravity(Gravity.CENTER_VERTICAL);
		return _llBackgroundView;
	}
	
	protected ImageView newImageView (Context _cContext){
		ImageView _ivImageView = new ImageView(_cContext);
		_ivImageView.setContentDescription("CellImage");
		LinearLayout.LayoutParams _lpImageViewParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		_lpImageViewParams.gravity = Gravity.CENTER_VERTICAL;
		_ivImageView.setLayoutParams(_lpImageViewParams);
		return _ivImageView;
	}

	private ImageView newAccessoryImageView (Context _cContext)
	{
		ImageView _ivAccessoryImageView = new ImageView(_cContext);
		_ivAccessoryImageView.setContentDescription("AccessoryImage");
		LinearLayout.LayoutParams _lpImaveViewParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		_lpImaveViewParams.gravity = Gravity.CENTER_VERTICAL|Gravity.RIGHT;
		_ivAccessoryImageView.setLayoutParams(_lpImaveViewParams);
		_ivAccessoryImageView.setPadding(m_nAccessoryPaddingLeftRight, m_nAccessoryPaddingTopBottom, m_nAccessoryPaddingLeftRight + m_nAccessoryPaddingRight, m_nAccessoryPaddingTopBottom);
		return _ivAccessoryImageView;
	}
	
	private LinearLayout newLinearLayout (Context _cContext)
	{
		LinearLayout _llTextContainer = new LinearLayout(_cContext);
		LinearLayout.LayoutParams _lpTextContainer = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		_llTextContainer.setLayoutParams(_lpTextContainer);
		_llTextContainer.setGravity(Gravity.CENTER_VERTICAL);
		_llTextContainer.setPadding(m_nTextContainerPaddingLeftRight, m_nZERO, m_nTextContainerPaddingLeftRight, m_nZERO);
		_llTextContainer.setOrientation(LinearLayout.VERTICAL);
		return _llTextContainer;
	}
	
	private LinearLayout newSpace (Context _cContext)
	{
		LinearLayout _vSpacer = new LinearLayout(_cContext);
		LinearLayout.LayoutParams _lpSpacerParams = new LinearLayout.LayoutParams (0, LayoutParams.MATCH_PARENT, 1.0f);
		_vSpacer.setLayoutParams(_lpSpacerParams);
		return _vSpacer;
	}
	
	private TextView newCellTextView (Context _cContext)
	{
		TextView _tvCellTextView = new TextView (_cContext);
		LinearLayout.LayoutParams _lpTextViewParams = new LinearLayout.LayoutParams (LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		_tvCellTextView.setLayoutParams(_lpTextViewParams);
		_tvCellTextView.setGravity(Gravity.CENTER_VERTICAL);
		_tvCellTextView.setEllipsize(TruncateAt.END);
		_tvCellTextView.setSingleLine();
		_tvCellTextView.setVisibility(GONE);
		return _tvCellTextView;
	}
	
	private TextView newTextView (Context _cContext)
	{
		TextView _tvTextView = newCellTextView(_cContext);
		_tvTextView.setTextColor(m_cslTextColor);
		_tvTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, m_nTextViewTextSize);
		_tvTextView.setTypeface(null, Typeface.BOLD);
		return _tvTextView;
	}
	
	private TextView newDetailTextView (Context _cContext)
	{
		TextView _tvTextView = newCellTextView(_cContext);
		_tvTextView.setTextColor(m_cslDetailTextColor);
		_tvTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, m_nDetailTextViewTextSize);
		_tvTextView.setTypeface(null, Typeface.NORMAL);	
		
		return _tvTextView;
	}
	
	
	
	/**
	 * @category TextView
	 */

	public TextView getTextView()
	{
		if (m_tvTextView==null){
			m_tvTextView = newTextView(m_cContext);
			getImageView();
			getContentView().addView(m_tvTextView);
		}
		return m_tvTextView;
	}

	public void setTextViewText(String _szTitle)
	{
		if (TextUtils.isEmpty(_szTitle)) {
			getTextView().setVisibility(View.GONE);
		} else {
			getTextView().setVisibility(View.VISIBLE);
			getTextView().setText(_szTitle);
		}
	}
	
	/**
	 * @category DetailTextView
	 */

	public TextView getDetailTextView() 
	{
		if (m_tvDetailTextView==null){
			m_tvDetailTextView = newDetailTextView(m_cContext);
			getTextView();
			getContentView().addView(m_tvDetailTextView);
		}
		
		return m_tvDetailTextView;
	}

	public void setDetailTextViewText(String _szDetailText) 
	{
		if (TextUtils.isEmpty(_szDetailText)) {
			getDetailTextView().setVisibility(View.GONE);
		} else {
			getDetailTextView().setVisibility(View.VISIBLE);
			getDetailTextView().setText(_szDetailText);
		}
	}
	
	/**
	 * @category ImageView
	 */

	public ImageView getImageView() 
	{
		if(m_ivImageView==null){
			m_ivImageView = newImageView(m_cContext);
			getBackgroundView().addView(m_ivImageView);
		}
		return m_ivImageView;
	}
	
	
	public void setImageViewImage (Object image)
	{	
		if (image == null) {
			getImageView().setVisibility(View.GONE);
		} else {
			getImageView() .setVisibility(View.VISIBLE);
			
			if ((image instanceof Integer)){
				getImageView().setImageResource((Integer)image);
			}else if ((image instanceof Drawable)){
				getImageView().setImageDrawable((Drawable)image);
			}else if ((image instanceof Bitmap)){
				getImageView().setImageBitmap((Bitmap)image);
			}
			
		}
	}

	
	/**
	 * @category AcessoryView
	 */

	public ImageView getAccessoryView() 
	{
		if(m_ivAccessoryImage==null){
			m_ivAccessoryImage = newAccessoryImageView(m_cContext);
			getContentView();
			getBackgroundView().addView(newSpace(m_cContext));
			getBackgroundView().addView(m_ivAccessoryImage);
			
			// Increase the touchable area for accessoryView
			post(getTouchDelegateAction(this, m_ivAccessoryImage, 30, 30, 30, 30));
			
			m_ivAccessoryImage.setOnClickListener(new OnClickListener() {
	            @Override
	            public void onClick(View v) {
	            	accessoryTouched();
	            }
	        });
		}
		return m_ivAccessoryImage;
	}

	public void setAccessoryImage(Object image) 
	{
		if (image == null) {
			getAccessoryView().setVisibility(View.GONE);
		} else {
			getAccessoryView().setVisibility(View.VISIBLE);
					
			if ((image instanceof Integer)){
				getAccessoryView().setImageResource((Integer)image);
			}else if ((image instanceof Drawable)){
				getAccessoryView().setImageDrawable((Drawable)image);
			}else if ((image instanceof Bitmap)){
				getAccessoryView().setImageBitmap((Bitmap)image);
			}
			
		}
	}

	
	/**
	 * @category Content_View
	 */
	
	public LinearLayout getContentView ()
	{
		if (m_llContentView==null){
			m_llContentView = newLinearLayout(m_cContext);
			getImageView();
			getBackgroundView().addView(m_llContentView);
		}
		return m_llContentView;
	}
	
	
	
	/**
	 * @category Background
	 */
	
	public LinearLayout getBackgroundView ()
	{
		if (m_llBackgroundView==null){
			m_llBackgroundView = newBackgroundView(m_cContext);
			this.addView(m_llBackgroundView);
		}
		return m_llBackgroundView;
	}
	
	public void setCellBackgroundColor (int _nBackgroundColor)
	{
		m_arrnBackgroundColor = new int [] {_nBackgroundColor, _nBackgroundColor};
	}
	
	public void setCellBackgroundGradient (int _nTopBackgroundColor, int _nBottomBackgroundColor)
	{
		m_arrnBackgroundColor = new int [] {_nTopBackgroundColor, _nBottomBackgroundColor};
	}
	
	/**
	 * @category IndexPath
	 */
	
	public void prepareCellToDisplayAtIndexPath (IndexPath indexPath)
	{
		setIndexPath(indexPath);
		setDefaultBackgroundColor();
		resetSelectedStatus();
	}
	public void setIndexPath (IndexPath indexPath){
		this.indexPath = indexPath;
	}
	
	public IndexPath getIndexPath ()
	{
		return this.indexPath;
	}
	
	/**
	 * @category Style_Application
	 */

	public void setDefaultBackgroundColor() 
	{
		setCellStyle();
	}

	@SuppressWarnings("deprecation")
	public void setCellStyle() 
	{
		TableViewCellStateDrawable tbv = new TableViewCellStateDrawable();
		
		if (tableView.styleIsGrouped()){
			if (indexPath != null){
				// Assign the right backgroundDrawable according to the cell's position in the group
				int _PaddingLeftRight = AutoResizer.resizeRelativeToOriginalWidth(10);
				Drawable backgroundDrawable;
				int bottomInset = 0;
				int topInset = 0;
				
				if (indexPath.getRowsInSection() == 1) { // if only cell in section, round all corners
					backgroundDrawable = tbv.createGradientDrawable(m_fCORNER_RADIUS, m_fCORNER_RADIUS, m_arrnBackgroundColor, m_nBorderColor);
					topInset = _PaddingLeftRight;
					bottomInset = _PaddingLeftRight;
				} else {
					if (indexPath.getRow() == 0) {// if first cell in section, round top corners
						backgroundDrawable = tbv.createGradientDrawable(m_fCORNER_RADIUS, m_nZERO, m_arrnBackgroundColor, m_nBorderColor);
						topInset = _PaddingLeftRight;
					} else if (indexPath.isLastCellInSection()) { // if last cell in section, round bottom corners
						backgroundDrawable = tbv.createGradientDrawable(m_nZERO, m_fCORNER_RADIUS, m_arrnBackgroundColor, m_nBorderColor);
						bottomInset = _PaddingLeftRight;
					} else { // do not round corners
						backgroundDrawable = tbv.createGradientDrawable(m_nZERO, m_nZERO, m_arrnBackgroundColor, m_nBorderColor);
					}
				}

				setBackgroundDrawable(new InsetDrawable(backgroundDrawable, _PaddingLeftRight, topInset, _PaddingLeftRight, bottomInset));

			}
		}else if (tableView.styleIsPlain()){
			setBackgroundDrawable( tbv.createGradientDrawable(m_nZERO, m_nZERO, m_arrnBackgroundColor, Color.TRANSPARENT));
		}
	}
	
	
	/**
	 * @category Selection_Style
	 */
	
	public TableViewCellSelectionStyle getCellSelectionStyle()
	{
		return m_cssSelectionStyle;
	}
	
	private void setCellSelectionStyle(TableViewCellSelectionStyle _cssStyle)
	{
		m_cssSelectionStyle = _cssStyle;
	}
	
	public void setCellSelectionStyleBlue ()
	{
		setCellSelectionStyle(TableViewCellSelectionStyle.Blue);
	}
	
	public void setCellSelectionStyleGrey ()
	{
		setCellSelectionStyle(TableViewCellSelectionStyle.Gray);
	}
	
	public void setCellSelectionStyleNone ()
	{
		setCellSelectionStyle(TableViewCellSelectionStyle.None);
	}
	
	public void setCellSelectionStyleCustomWithColor (int _nSelectionColor)
	{
		setCellSelectionStyle(TableViewCellSelectionStyle.Custom);
		m_arrnCustomCellSelectionStyle = _nSelectionColor;
	}
	
	public boolean cellSelectionStyleIsBlue()
	{
		return (m_cssSelectionStyle == TableViewCellSelectionStyle.Blue);
	}
	
	public boolean cellSelectionStyleIsGrey()
	{
		return (m_cssSelectionStyle == TableViewCellSelectionStyle.Gray);
	}
	
	public boolean cellSelectionStyleIsNone()
	{
		return (m_cssSelectionStyle == TableViewCellSelectionStyle.None);
	}
	
	public boolean cellSelectionStyleIsCustom()
	{
		return (m_cssSelectionStyle == TableViewCellSelectionStyle.Custom);
	}
	
	private int colorFromCellSelectionStyle()
	{
		if (cellSelectionStyleIsBlue()){
			return m_nBlueStyleColor;
		}else if (cellSelectionStyleIsGrey()){
			return m_nGreyStyleColor;
		}else if (cellSelectionStyleIsNone()){
			return Color.TRANSPARENT;
		}else if (cellSelectionStyleIsCustom()){
			return m_arrnCustomCellSelectionStyle;
		}
		return 0;
	}
	
	/**
	 * @category Selected Status
	 */
	
	public void setSelectedStatus (boolean _bSelectedStatus)
	{
		if (_bSelectedStatus == true){
			m_ssSelectionStatus = SelectionStatus.Selected;	
		}else{
			m_ssSelectionStatus = SelectionStatus.Unselected;
		}
		displaySelection();
	}
	
	public void resetSelectedStatus ()
	{
		if (tableView.cellIsSelectedForIndexPath(indexPath)){
			setSelected();
		}else{
			setUnselected();
		}
	}
	
	public void setSelected ()
	{
		setSelectedStatus (true);
		displaySelection();
	}
	
	public void setUnselected ()
	{
		setSelectedStatus (false);
		displayUnselection();
	}
	
	public void flipSelection ()
	{
		if (isSelected()){
			setUnselected();
		}else{
			setSelected();
		}
	}
	
	public boolean isSelected()
	{
		return (m_ssSelectionStatus == SelectionStatus.Selected);
	}
	
	protected void displaySelection()
	{
		if ((tableView.cellShouldSelect())&&(selectedView!=null)){
           selectedView.getBackground().setColorFilter(colorFromCellSelectionStyle(),Mode.SRC_ATOP);
           selectedView.invalidate();
            }
	}
	
	protected void displayUnselection()
	{
		if ((tableView.cellShouldSelect())&&(selectedView!=null)){
			selectedView.getBackground().clearColorFilter();
			selectedView.invalidate();
		}
	}
	
	protected void displayOppositeSelection ()
	{
		if (isSelected()){
			displayUnselection();
		}else{
			displaySelection();
		}
	}
	
	protected void displaySelectedForCurrentStatus ()
	{
		if (isSelected()){
			displaySelection();
		}else{
			displayUnselection();
		}		
	}
	
	
	/**
	 * @category AccessoryImage
	 */
	
	

	/**
	 * @category Touch_Delegate
	 */

	/**
	 * Adds a touchable padding around a View by constructing a TouchDelegate and adding it to parent View.
	 * 
	 * @param parent
	 *            The "outer" parent View
	 * @param delegate
	 *            The delegate that handles the TouchEvents
	 * @param topPadding
	 *            Additional touch area in pixels above View
	 * @param bootomPadding
	 *            Additional touch area in pixels below View
	 * @param topPadding
	 *            Additional touch area in pixels left to View
	 * @param topPadding
	 *            Additional touch area in pixels right to View
	 * @return A runnable that you can post as action to a Views event queue
	 */
	
	
	private static Runnable getTouchDelegateAction(final View parent, final View delegate, final int topPadding, final int bottomPadding, final int leftPadding, final int rightPadding) 
	{
		return new Runnable() {
			@Override
			public void run() {
				// Construct a new Rectangle and let the Delegate set its values
				Rect touchRect = new Rect();
				delegate.getHitRect(touchRect);

				// Modify the dimensions of the Rectangle
				// Padding values below zero are replaced by zeros
				touchRect.top -= Math.max(0, topPadding);
				touchRect.bottom += Math.max(0, bottomPadding);
				touchRect.left -= Math.max(0, leftPadding);
				touchRect.right += Math.max(0, rightPadding);

				// Now we are going to construct the TouchDelegate
				TouchDelegate touchDelegate = new TouchDelegate(touchRect, delegate);

				// And set it on the parent
				parent.setTouchDelegate(touchDelegate);
			}
		};
	}
	
	/**
	 * @category Event_Handlers
	 */
	
	private void accessoryTouched ()
	{
		tableView.m_dDelegate.accessoryButtonTappedForRowWithIndexPathForTableView(indexPath, tableView);
	}
	
	private void cellTouched ()
	{
		tableView.cellWasSelectedAtIndexPath(this, indexPath);
	}
	
	//Nested inner-class responsible for drawing grouped TableViewCells
	class TableViewCellStateDrawable extends StateListDrawable
	{
		public TableViewCellStateDrawable(){}

		public TableViewCellStateDrawable(float topRadius, float bottomRadius, int[] _arrColorGradientStateNormal, int[] _arrColorGradientStatePressed, int _nBorderColor) 
		{
			addState(new int[] { -android.R.attr.state_pressed }, createGradientDrawable(topRadius, bottomRadius, _arrColorGradientStateNormal, _nBorderColor));
			addState(new int[] { android.R.attr.state_pressed }, createGradientDrawable(topRadius, bottomRadius, _arrColorGradientStatePressed, _nBorderColor));
		}

		public GradientDrawable createGradientDrawable(float topRadius, float bottomRadius, int[] color, int borderColor) 
		{
			GradientDrawable gradientDrawable = new GradientDrawable(Orientation.TOP_BOTTOM, color);
			gradientDrawable.setShape(GradientDrawable.RECTANGLE);
			gradientDrawable.setGradientRadius(270.0f);
			gradientDrawable.setStroke(1, borderColor);
			gradientDrawable.setCornerRadii(getRadii(topRadius, bottomRadius));

			return gradientDrawable;
		}
		
		public GradientDrawable createGradientDrawable(int[] color) 
		{
			GradientDrawable gradientDrawable = new GradientDrawable(Orientation.TOP_BOTTOM, color);
			gradientDrawable.setShape(GradientDrawable.RECTANGLE);
			gradientDrawable.setStroke(1, Color.TRANSPARENT);

			return gradientDrawable;
		}

		private float[] getRadii(float top, float bottom) 
		{
			return new float[] { top, top, //
					top, top, //
					bottom, bottom, //
					bottom, bottom //
			};
		}
	}

}




package co.bytemark.android.opentools.tableview;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * TableView adapter used to interface with Android ListView.
 */
public class TableViewAdapter extends BaseAdapter 
{

	private static final int VIEW_TYPE_HEADER = 0;
	private static final int VIEW_TYPE_CELL = 1;
	private static final int VIEW_TYPE_FOOTER = 2;

	private final Context m_context;
	private final TableView m_TableView;
	
	private Map<Integer, IndexPath> m_hmIndexPathsMap = new HashMap<Integer, IndexPath>();
	private Map<Integer, Integer> m_hmRowsInSection = new HashMap<Integer, Integer>();
	private int m_nCachedListCount = -1;

	public TableViewAdapter(Context context, TableView uiTableView) 
	{
		this.m_context = context;
		this.m_TableView = uiTableView;
	}

	@Override
	public int getViewTypeCount() 
	{
		return 4;
	}

	@Override
	public int getItemViewType(int position) 
	{
		IndexPath indexPath = retrieveIndexPathByPosition(position);
		if (indexPath == null)
			return IGNORE_ITEM_VIEW_TYPE;
		else if (indexPath.isHeader())
			return VIEW_TYPE_HEADER;
		else if (indexPath.isFooter())
			return VIEW_TYPE_FOOTER;
		else
			return VIEW_TYPE_CELL;
	}

	@Override
	public int getCount() 
	{
		int countItems;
		
		if (m_nCachedListCount<0){
			int numberOfSections = m_TableView.m_dDataSource.numberOfSectionsInTableView(m_TableView);
			countItems = numberOfSections * 2; // Start with a count equivalent for header and footer for each section
			m_hmRowsInSection.clear();
			for (int section = 0; section < numberOfSections; section++) {
				int rowsForSection = m_TableView.m_dDataSource.numberOfRowsInSectionForTableView(section, m_TableView);
				m_hmRowsInSection.put(section, rowsForSection);
				countItems += rowsForSection;
			}
			m_nCachedListCount = countItems;
		}else {
			countItems = m_nCachedListCount;
		}
		
		return countItems;
	}
	
	
	

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getItem(int position) {
		return getView (position, null, null);
	}
	
	public View getItemAtIndexPath(IndexPath indexPath) {
		return getViewForIndexPath (indexPath, null, null);
	}

	@Override
	public View getView(int position, View reusableView, ViewGroup parent) 
	{
		IndexPath indexPath = retrieveIndexPathByPosition(position);
		if (indexPath == null) {
			throw new NullPointerException("Unable to retrieve index path for position ");
		} else {
			View _vTableViewItem = getViewForIndexPath (indexPath, reusableView, parent);			
			return _vTableViewItem;
		}
	}
	
	public View getViewForIndexPath (IndexPath indexPath, View reusableView, ViewGroup parent)
	{
		if (indexPath == null) {
			throw new NullPointerException("Unable to retrieve index path for position ");
		} else if (indexPath.isHeader()) {
			TableViewHeader headerView = (TableViewHeader) m_TableView.m_dDelegate.viewForHeaderInSectionWithReusableViewForTableView(indexPath.getSection(), reusableView, m_TableView);
			headerView =  (TableViewHeader) m_TableView.m_dDataSource.viewForHeaderWithTitleInSectionWithReusableViewForTableView(indexPath.getSection(), headerView, m_TableView);
			if (headerView==null){
				System.out.println("Making Empty Header");
				headerView = new TableViewHeader(m_context, m_TableView, true );
			}
			m_TableView.m_dDelegate.willDisplayHeaderViewForSectionForTableView(headerView, indexPath.getSection(), m_TableView);
			return headerView;
			
		} else if (indexPath.isFooter()){
			TableViewFooter footerView = (TableViewFooter) m_TableView.m_dDelegate.viewForFooterInSectionWithReusableViewForTableView(indexPath.getSection(), reusableView, m_TableView);
			footerView =  (TableViewFooter) m_TableView.m_dDataSource.viewForFooterWithTitleInSectionWithReusableViewForTableView(indexPath.getSection(), footerView, m_TableView);
			if (footerView==null){
				footerView = new TableViewFooter(m_context, m_TableView, true);
			}
			m_TableView.m_dDelegate.willDisplayFooterViewForSectionForTableView(footerView, indexPath.getSection(), m_TableView);
			return footerView;
			
		}else{
			TableViewCell cellView = (TableViewCell) m_TableView.m_dDataSource.cellForRowAtIndexPathWithReusableViewForTableView(indexPath, reusableView, m_TableView);

			if (cellView!=null){
				cellView.prepareCellToDisplayAtIndexPath(indexPath);
			}
			m_TableView.m_dDelegate.willDisplayCellAtIndexPathForTableView(cellView, indexPath, m_TableView);

			return cellView; 
		}
	}

	/**
	 * Retrieve an {@link IndexPath} according to a ListView's item position.
	 * 
	 * @param position
	 * @return An {@link IndexPath} if position is valid, <code>null</code> else
	 */
	
	public IndexPath retrieveIndexPathByPosition(final int position) 
	{
		IndexPath indexPath = m_hmIndexPathsMap.get(position);
		if (indexPath == null) {
			int _nNumberOfSections = m_hmRowsInSection.size();
			
			int numberOfRowsBefore = 0;
			for (int _nSection = 0; _nSection < _nNumberOfSections; _nSection++) {
				int _nNumberOfRows = m_hmRowsInSection.get(_nSection);
				if (position == 0) { // Shortcut for the first item
					indexPath = new IndexPath(0, -1, _nNumberOfRows, _nNumberOfSections);
					break;
				}if (position == numberOfRowsBefore) { // Header
					indexPath = new IndexPath(_nSection, -1, _nNumberOfRows, _nNumberOfSections);
					break;
				}else if (position <= numberOfRowsBefore + _nNumberOfRows) { // Cell
					int _nRow = position - numberOfRowsBefore - 1;
					indexPath = new IndexPath(_nSection, _nRow, _nNumberOfRows, _nNumberOfSections);
					break;
				}else if (position == numberOfRowsBefore + _nNumberOfRows+1){ // Footer
					int _nRow = _nNumberOfRows;
					indexPath = new IndexPath(_nSection, _nRow, _nNumberOfRows, _nNumberOfSections);
					break;
				}
				
				// This position doesn't fit to this group, see the next one
				numberOfRowsBefore += _nNumberOfRows + 2; // rows + header
			}
			m_hmIndexPathsMap.put(position, indexPath);
		}
		return indexPath;
	}
	
	public int positionForIndexPath(IndexPath indexPath){
		Set<Integer> keySet = m_hmIndexPathsMap.keySet();
		for (Integer key : keySet) {
			if (indexPath.equals(m_hmIndexPathsMap.get(key))){
				return key;
			}
		} 
		return -1;
	}

	@Override
	public void notifyDataSetChanged() 
	{
		resetDataStructure();
	}

	@Override
	public void notifyDataSetInvalidated() 
	{
		resetDataStructure();
	}
	
	protected void resetDataStructure (){
		m_nCachedListCount = -1;
		m_hmIndexPathsMap.clear();
		m_hmRowsInSection.clear();
		super.notifyDataSetInvalidated();
	}
	

}
package co.bytemark.android.opentools.tableview;

import android.view.View;
import co.bytemark.android.opentools.delegatehelper.DelegateHelper;


/**
 * <b>Dependencies:</b>
 * <br> � co.bytemark.android.opentools.delegatehelper
 * <br>
 * <br><b>Description:</b>
 * <br>The TableViewDataSource protocols are adopted by an object that mediates the application's data model for a {@link TableView} object. The data source provides the table view object with the information it needs to construct and modify a table view.
 * <br>
 * <br>As a representative of the data model, the data source supplies minimal information about the table view�s appearance. The table view object�s delegate�an object adopting the TableViewDelegate protocol�provides that information.
 * <br>
 * <br>The required methods of the protocol provide the cells to be displayed by the table view as well as inform the TableView object about the number of sections and the number of rows in each section. The data source may implement optional methods to configure various aspects of the table view and to insert, delete, and reorder rows.
 * 
 */

public class TableViewDataSource extends DelegateHelper 
{
	public TableViewDataSource(Object delegateInstance) 
	{
		super(delegateInstance);
	}
	
	/**
	 * @category Configuring a Table View
	 */
	
	/**
	 * <br><b>Contingency:</b>
	 * <br><i>Optional Delegate Method</i>: Will default to <code>1</code> section.
	 * <br>
	 * <br><b>Delegate Method Called:</b>
	 * <br><code>public Integer numberOfSectionsInTableView (TableView uiTableView)</code>
	 * <br>
	 * <br><b>Description:</b> 
	 * <br>Queries DataSource delegate for an Integer specifying the number of sections for the TableView. 
	 * <br>
	 * @param tableView
	 * @return Integer specifying the number of sections
	 */
	
	public Integer numberOfSectionsInTableView (TableView tableView)
	{
		String methodName = "numberOfSectionsInTableView";
		Object _arrParameters[] = {tableView};
		
		if(this.respondsToMethodWithName(methodName)){
			return (Integer) performMethodNamedWithParameters(methodName, _arrParameters);
		}
		return 1;
	}
	
	/**
	 * <br><b>Contingency:</b>
	 * <br><i><u>Required</u> Delegate Method</i>
	 * <br>
	 * <br><b>Delegate Method Called:</b>
	 * <br><code>public Integer numberOfRowsInSectionForTableView (Integer section, TableView uiTableView)</code>
	 * <br>
	 * <br><b>Description:</b> 
	 * <br>Queries DataSource delegate for an Integer specifying the number of rows for the given section of the TableView. 
	 * <br>
	 * @param section
	 * @param tableView
	 * @return Integer specifying the number of rows
	 */
	
	public Integer numberOfRowsInSectionForTableView (Integer section, TableView tableView)
	{
		String methodName = "numberOfRowsInSectionForTableView";
		Object _arrParameters[] = {section, tableView};
		
		if(this.respondsToMethodWithName(methodName)){
			return (Integer) performMethodNamedWithParameters(methodName, _arrParameters);
		}else{
			throw new NullPointerException("Required method named \"" +methodName + "\" is not implemented in class \"" + tableView.m_dDataSource.getClass().getName() + "\". Cannot get row count for section  " + section+".");
		}
	}
	
	/**
	 * <br><b>Contingency:</b>
	 * <br><i><u>Required</u> Delegate Method</i>
	 * <br>
	 * <br><b>Delegate Method Called:</b>
	 * <br><code>public View cellForRowAtIndexPathWithReusableViewForTableView (IndexPath indexPath, View reusableView, TableView tableView)</code>
	 * <br>
	 * <br><b>Description:</b> 
	 * <br>Queries DataSource delegate for a view containing content to be displayed for a given IndexPath. 
	 * <br>
	 * @param indexPath
	 * @param reusableView
	 * @param tableView
	 * @return View of subclass TableViewCell
	 */
	
	public View cellForRowAtIndexPathWithReusableViewForTableView (IndexPath indexPath, View reusableView, TableView tableView)
	{
		String methodName = "cellForRowAtIndexPathWithReusableViewForTableView";
		Object _arrParameters[] = {indexPath, reusableView, tableView};
		
		if(this.respondsToMethodWithName(methodName)){
			return (View) performMethodNamedWithParameters(methodName, _arrParameters);
		}else{
			throw new NullPointerException("Required method named \"" +methodName + "\" is not implemented in class \"" + tableView.m_dDataSource.getClass().getName() + "\". Cannot get row count for indexPath:  " + indexPath.toString() +".");
		}
	}
	
	
	/**
	 * <br><b>Contingency:</b>
	 * <br><i>Optional Delegate Method</i>: Will default to empty header
	 * <br>
	 * <br><b>Delegate Method Called:</b>
	 * <br><code>public String titleForHeaderInSectionForTableView (Integer section, TableView tableView)</code>
	 * <br>
	 * <br><b>Description:</b> 
	 * <br>Queries DataSource delegate for a String title and places in current Header View. If a HeaderView does not exist for the current section, one will be created. Returning a <code>null</code> string will remove the header from the section. 
	 * <br>
	 * 
	 * @param section
	 * @param reusableView
	 * @param tableView
	 * @return TableViewHeader with String title set
	 */
	
	public View viewForHeaderWithTitleInSectionWithReusableViewForTableView (Integer section, View reusableView, TableView tableView)
	{
		String methodName = "titleForHeaderInSectionForTableView";
		Object _arrParameters[] = {section, tableView};
		
		TableViewHeader headerView = (TableViewHeader)reusableView;
		
		if(this.respondsToMethodWithName(methodName)){
			String _szTitle = (String) performMethodNamedWithParameters(methodName, _arrParameters);
			if (_szTitle!=null){
				System.out.println("Making Hidden Header");
				
				if (headerView == null){
					headerView = new TableViewHeader(tableView.m_cContext, tableView, true);
				}
				headerView.setTitle(_szTitle);
			}
		}
			
		return headerView;
	}

	
	/**
	 * <br><b>Contingency:</b>
	 * <br><i>Optional Delegate Method</i>: Will default to empty footer
	 * <br>
	 * <br><b>Delegate Method Called:</b>
	 * <br><code>public String titleForFooterInSectionForTableView (Integer section, TableView tableView)</code>
	 * <br>
	 * <br><b>Description:</b> 
	 * <br>Queries DataSource delegate for a String title and places in current footer view. If a footer view does not exist for the current section, one will be created. Returning a <code>null</code> string will remove the footer from the section. 
	 * <br>
	 * 
	 * @param section
	 * @param reusableView
	 * @param tableView
	 * @return TableViewFooter with String title set
	 */
	
	
	public View viewForFooterWithTitleInSectionWithReusableViewForTableView (Integer section, View reusableView, TableView tableView)
	{
		String methodName = "titleForFooterInSectionForTableView";
		Object _arrParameters[] = {section, tableView};
		
		TableViewFooter footerView = (TableViewFooter)reusableView;
		
		if(this.respondsToMethodWithName(methodName)){
			String _szTitle = (String) performMethodNamedWithParameters(methodName, _arrParameters);
			if (_szTitle!=null){
				if (footerView == null){
					footerView = new TableViewFooter(tableView.m_cContext, tableView, false);
				}
				footerView.setTitle(_szTitle);
			}
		}
		return footerView;
	}
	
	
	/**
	 * @category Inserting or Deleting Table Rows
	 */
	
	/**
	 * <br><b>Contingency:</b>
	 * <br><i>Optional Delegate Method</i>: Only used when putting TableView in edit-mode
	 * <br>
	 * <br><b>Delegate Method Called:</b>
	 * <br><code></code>
	 * <br>
	 * <br><b>Description:</b> 
	 * <br>Queries DataSource delegate for a boolean value  
	 * <br>
	 * 
	 * @param section
	 * @param reusableView
	 * @param tableView
	 * @return Boolean value determining weather a cell at an IndexPath can be edited
	 */
	
	
	/**
	 * How to get insert delete to work?
	 * tableView:commitEditingStyle:forRowAtIndexPath:
	 */
}
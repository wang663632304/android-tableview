package co.bytemark.android.opentools.tableview;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;

/**
 * <b>Dependencies</b>
 * <br> autoresizer, delegatehelper
 * <br>
 * <br>Description:</b>
 * <br>An instance of TableView is a means for displaying and editing hierarchical lists of information.
 *
 */

public class TableView extends ListView  
{
	public Context m_cContext;
	public TableViewAdapter tableViewAdapterInternal;
	public static enum TableViewStyle {
	    TableViewStylePlain, TableViewStyleGrouped
	}
	public TableViewStyle m_sTableViewStyle = TableViewStyle.TableViewStylePlain;
	
	public TableViewDataSource m_dDataSource;
	public TableViewDelegate m_dDelegate;

	public static enum TableViewSelectionMode {NoSelection, TemporarySelection, SingleSelection, MultiSelection}
	public TableViewSelectionMode m_tvssTableViewSelectionMode = TableViewSelectionMode.TemporarySelection;
	protected ArrayList<IndexPath> m_alSelectedCells;
	protected IndexPath m_SelectedSingleCell = null;
	
	private View m_vTableViewHeader;
	private View m_vTableViewFooter;
	
	private int m_nBackgroundColor = -1;
	
	public TableView(Context context) 
	{
		super(context);
	}
	
	public TableView(Context context, Object datasource, Object delegate) 
	{
		super(context);
		
		setDataSourceAndDelegate(context, datasource, delegate);
	}

	public TableView(Context context, AttributeSet attrs, int defStyle) 
	{
		super(context, attrs, defStyle);
	}

	public TableView(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
	}

	private void init() 
	{
		setChoiceMode(1);
		
		setDivider(new ColorDrawable(Color.TRANSPARENT));
		setSelector(new ColorDrawable(Color.TRANSPARENT));

		
		if(styleIsGrouped()){
			setGroupedStyle();
		}else if (styleIsPlain()){
			setPlainStyle();
		}

	}
	
	private void setGroupedStyle ()
	{
		if (m_nBackgroundColor<=0){
			setBackgroundColor(m_nBackgroundColor);
		}else{
			setBackgroundColor(Color.parseColor("#FFC5CBD2"));
		}
		
	}
	
	private void setPlainStyle ()
	{
		if (m_nBackgroundColor<=0){
			setBackgroundColor(m_nBackgroundColor);
		}else{
			setBackgroundColor(Color.TRANSPARENT);
		}
	}
	
	public void setBackgroundColor (int color){
		m_nBackgroundColor = color;
		super.setBackgroundColor(color);
	}
	

	
	public void setDataSource (Context context, Object _oDataSource)
	{
		setDataSourceAndDelegate(context, _oDataSource, null);
	}
	
	public void setDelegate (Context context, Object _oDelegate)
	{
		setDataSourceAndDelegate(context, null, _oDelegate);
	}
	
	public void setDataSourceAndDelegate (Context context, Object _oDataSource, Object _oDelegate)
	{
		this.m_cContext = context;
		
		if(_oDataSource !=null){
			this.m_dDataSource = new TableViewDataSource(_oDataSource);
		}
		if(_oDelegate!=null){
			this.m_dDelegate = new  TableViewDelegate(_oDelegate);
		}
				
		if ((this.m_dDataSource!=null) && (this.m_dDelegate!=null)){
			setAdapter();
			init ();
		}
	}

	public void setAdapter() 
	{
		tableViewAdapterInternal = new TableViewAdapter(m_cContext, this);	
		super.setAdapter(tableViewAdapterInternal);
	}
	
	public TableViewAdapter getAdapter ()
	{
		return tableViewAdapterInternal;
	}
	
	
	/**
	 * @category Appearance
	 */
	
	public void setTableViewStyle(TableViewStyle style)
	{
		this.m_sTableViewStyle = style;
		init();
	}
	
	public void setTableViewStyleGrouped()
	{
		setTableViewStyle(TableViewStyle.TableViewStyleGrouped);
	}
	
	public void setTableViewStylePlain()
	{
		setTableViewStyle(TableViewStyle.TableViewStylePlain);
	}
	
	public TableViewStyle getTableViewStyle()
	{
		return m_sTableViewStyle;
	}
	
	public boolean styleIsGrouped ()
	{
		return (m_sTableViewStyle == TableViewStyle.TableViewStyleGrouped);
	}
	
	public boolean styleIsPlain ()
	{
		return (m_sTableViewStyle == TableViewStyle.TableViewStylePlain);
	}

	
	/**
	 * @category Accessing_Cells_and_Sections
	 */
	
	public View viewForRowAtIndexPath(IndexPath indexPath)
	{
		return this.tableViewAdapterInternal.getItemAtIndexPath(indexPath);
	}
	
	/**
	 * @category Accessing Header and Footer Views
	 * � headerViewForSection:
	 * � footerViewForSection:
	 */
	
	public void setTableViewHeader (View _vgTableViewHeader)
	{
		m_vTableViewHeader = _vgTableViewHeader;
		this.addHeaderView(m_vTableViewHeader);
	}
	
	public void setTableViewFooter (View _vgTableViewFooter)
	{
		m_vTableViewFooter = _vgTableViewFooter;
		this.addFooterView(m_vTableViewFooter);
	}
	
	/**
	 * @category Managing Selections
	 * � indexPathForSelectedRow
	 * � indexPathsForSelectedRows
	 * � selectRowAtIndexPath
	 * � deselectRowAtIndexPath
	 * - SetSelection style
	 * 
	 */
	
	public void cellWasSelectedAtIndexPath(TableViewCell cellView, IndexPath indexPath){
		if (cellShouldSelect()){
			if (selectionModeIsSingle()){
				performSingleSelection(cellView, indexPath);
			}else if (selectionModeIsMulti()){
				performMultiSelection(cellView, indexPath);
			}else if(getSelectionMode() == TableView.TableViewSelectionMode.TemporarySelection){
				performTemporarySelection(cellView, indexPath);
			}
		}
	}
	
	private void performSingleSelection (TableViewCell cellView, IndexPath indexPath){
		
		if	(cellIsSameAsSelectedWithIndexPath(indexPath)){
			m_dDelegate.willSelectRowAtIndexPathForTableView(indexPath, this);
			cellView.setSelected();
		}else{
			if(m_SelectedSingleCell!=null){
				m_dDelegate.willDeselectRowAtIndexPathForTableView(m_SelectedSingleCell, this);
			}
			m_dDelegate.willSelectRowAtIndexPathForTableView(indexPath, this);
			
			cellView.setSelected();
			if(m_SelectedSingleCell!=null){
				int position = tableViewAdapterInternal.positionForIndexPath(m_SelectedSingleCell);
				if	(position>=1){
					TableViewCell deselectionCell = (TableViewCell)viewForPosition(position);
					if(deselectionCell!=null){
						deselectionCell.setUnselected();
					}
				}
				m_dDelegate.didDeselectRowAtIndexPathForTableView(m_SelectedSingleCell, this);
			}
			
			m_SelectedSingleCell = indexPath;
			m_dDelegate.didSelectRowAtIndexPathForTableView(indexPath, this);
		}
	}
	
	private boolean cellIsSameAsSelectedWithIndexPath (IndexPath indexPath){
		if	(m_SelectedSingleCell!=null){
			return indexPath.equals(m_SelectedSingleCell);
		}
		return false;
	}
	
	private void performMultiSelection (TableViewCell cellView, IndexPath indexPath){
		if (!cellView.isSelected()){
			m_dDelegate.willSelectRowAtIndexPathForTableView(indexPath, this);
			cellView.flipSelection();
			getSelectedCellsArray().add( indexPath);
			m_dDelegate.didSelectRowAtIndexPathForTableView(indexPath, this);
		}else{
			m_dDelegate.willDeselectRowAtIndexPathForTableView(indexPath, this);
			cellView.flipSelection();
			getSelectedCellsArray().remove(indexPath);
			m_dDelegate.didDeselectRowAtIndexPathForTableView(indexPath, this);
		}
	}
	
	private void performTemporarySelection (TableViewCell cellView, IndexPath indexPath){
		m_dDelegate.willSelectRowAtIndexPathForTableView(indexPath, this);	
		m_dDelegate.didSelectRowAtIndexPathForTableView(indexPath, this);	
		cellView.setUnselected();
		m_dDelegate.willDeselectRowAtIndexPathForTableView(indexPath, this);
	}
	
	public boolean cellIsSelectedForIndexPath (IndexPath indexPath){
		if (selectionModeIsSingle()){
			if	(m_SelectedSingleCell!=null){
				return (indexPath.equals(m_SelectedSingleCell));
			}
		}else if (selectionModeIsMulti()){
			return (getSelectedCellsArray().contains(indexPath));
		}
		return false;
	}
	
	public ArrayList<IndexPath> getSelectedCellsArray(){
		if (m_alSelectedCells==null){
			m_alSelectedCells = new ArrayList<IndexPath>();
		}
		return m_alSelectedCells;
	}
	
	public IndexPath getSelectedCell(){
		if (m_SelectedSingleCell!=null){
			return m_SelectedSingleCell;
		}
		return null;
	}
	
	private void setSelectionMode(TableViewSelectionMode _tvssTableViewSelectionMode)
	{
		m_tvssTableViewSelectionMode = _tvssTableViewSelectionMode;
	}
	public void setSelectionModeNoSelection (){
		setSelectionMode (TableViewSelectionMode.NoSelection);
	}
	
	public void setSelectionModeTemporarySelection(){
		setSelectionMode (TableViewSelectionMode.TemporarySelection);
	}
	
	public void setSelectionModeSingleSelection (){
		setSelectionMode (TableViewSelectionMode.SingleSelection);
	}
	
	public void setSelectionModeMultiSelection (){
		setSelectionMode (TableViewSelectionMode.MultiSelection);
	}
	
	public TableViewSelectionMode getSelectionMode()
	{
		return m_tvssTableViewSelectionMode;
	}
	
	public boolean cellsShouldStaySelected ()
	{
		return (selectionModeIsSingle()||selectionModeIsMulti());
		
	}
	
	public boolean cellShouldSelect(){
		return (getSelectionMode() != TableView.TableViewSelectionMode.NoSelection);
	}
	
	public boolean selectionModeIsSingle (){
		return (m_tvssTableViewSelectionMode==TableViewSelectionMode.SingleSelection);
	}
	
	public boolean selectionModeIsMulti (){
		return (m_tvssTableViewSelectionMode==TableViewSelectionMode.MultiSelection);
	}

	
	private View viewForPosition(int position){
		if (position>=getFirstVisiblePosition()){
	    return getChildAt(position - 
	       getFirstVisiblePosition());
	   	}
		return null;
	}
	
	/**
	 * @category Managing the Editing of Table Cells
	 * property: editing
	 * � setEditing:
	 */
	
	/**
	 * @category Reloading the Table View
	 * � reloadData
	 * http://stackoverflow.com/questions/2123083/android-listview-refresh-single-row
	 * � reloadRowsAtIndexPaths
	 * � reloadSections
	 */
	 
	 public void reloadData (){
	     tableViewAdapterInternal.resetDataStructure ();
	 }

	
}

package co.bytemark.android.opentools.tableview;

import co.bytemark.android.opentools.delegatehelper.DelegateHelper;
import android.view.View;

/**
 * <b>Dependencies:</b>
 * <br> � co.bytemark.android.opentools.delegatehelper
 * <br>
 * <br><b>Description:</b>
 * <br>The delegate of a TableView object must adopt the TableViewDelegate's protocol methods. Optional methods of the protocol allow the delegate to manage selections, configure section headings and footers, help to delete and reorder cells, and perform other actions.
 */

public class TableViewDelegate extends DelegateHelper {

	public TableViewDelegate(Object delegateInstance) {
		super(delegateInstance);
	}
	
	/**
	 * @category Configuring_Rows_for_the_TableView
	 */
	
	/**
	 * <br><b>Contingency:</b>
	 * <br><i>Optional Delegate Method</i>
	 * <br>
	 * <br><b>Delegate Method Called:</b>
	 * <br><code>public void willDisplayCellAtIndexPathForTableView (View cell, IndexPath indexPath,  TableView tableView)</code>
	 * <br>
	 * <br><b>Description:</b> 
	 * <br>Tells the delegate the table view is about to draw a cell for a particular row.
	 * <br>
	 * <br>A table view sends this message to its delegate just before it uses cell to draw a row, thereby permitting the delegate to customize the cell object before it is displayed. This method gives the delegate a chance to override state-based properties set earlier by the table view, such as selection and background color. After the delegate returns, the table view sets only the alpha and frame properties.
	 * @param cell
	 * @param indexPath
	 * @param tableView
	 */
	
	public void willDisplayCellAtIndexPathForTableView (View cell, IndexPath indexPath,  TableView tableView)
	{
		String methodName = "willDisplayCellAtIndexPathForTableView";
		Object _arrParameters[] = { cell, indexPath,  tableView};
		
		if(this.respondsToMethodWithName(methodName)){
			 performMethodNamedWithParameters(methodName, _arrParameters);
		}
	}
	
	/**
	 * @category Managing_Accessory_Views
	 */
	
	/**
	 * <br><b>Contingency:</b>
	 * <br><i>Optional Delegate Method</i>
	 * <br>
	 * <br><b>Delegate Method Called:</b>
	 * <br><code>public void accessoryButtonTappedForRowWithIndexPathForTableView (IndexPath indexPath,  TableView tableView)</code>
	 * <br>
	 * <br><b>Description:</b>
	 * <br>Tells the delegate that the user tapped the accessory (disclosure) view associated with a given row.
	 * <br>
	 * <br>The delegate usually responds to the tap on the disclosure button (the accessory view) by displaying a new view related to the selected row.
	 * @param indexPath
	 * @param tableView
	 */
	
	public void accessoryButtonTappedForRowWithIndexPathForTableView (IndexPath indexPath,  TableView tableView)
	{
		String methodName = "accessoryButtonTappedForRowWithIndexPathForTableView";
		Object _arrParameters[] = { indexPath,  tableView};
		
		if(this.respondsToMethodWithName(methodName)){
			 performMethodNamedWithParameters(methodName, _arrParameters);
		}
	}
	
	/**
	 * @category Managing_Selections
	 */
	
	/**
	 * <br><b>Contingency:</b>
	 * <br><i>Optional Delegate Method</i>
	 * <br>
	 * <br><b>Delegate Method Called:</b>
	 * <br><code>public void willSelectRowAtIndexPathForTableView (IndexPath indexPath,  TableView tableView)</code>
	 * <br>
	 * <br><b>Description:</b>
	 * <br>Tells the delegate that a specified row is about to be selected.
	 * <br>
	 * <br>This method is not called until users touch a row and then lift their finger; the row isn't selected until then, although it is highlighted on touch-down.
	 * <br>
	 * <br>This delegate method is not called if the TableView's selection mode is <code>TableViewSelectionModeNoSelection</code>.
	 * @param indexPath
	 * @param tableView
	 */
	
	public void willSelectRowAtIndexPathForTableView (IndexPath indexPath,  TableView tableView)
	{
		String methodName = "willSelectRowAtIndexPathForTableView";
		Object _arrParameters[] = {indexPath,  tableView};
		
		if(this.respondsToMethodWithName(methodName)){
			 performMethodNamedWithParameters(methodName, _arrParameters);
		}
	}

	/**
	 * <br><b>Contingency:</b>
	 * <br><i>Optional Delegate Method</i>
	 * <br>
	 * <br><b>Delegate Method Called:</b>
	 * <br><code>public void didSelectRowAtIndexPathForTableView (IndexPath indexPath,  TableView tableView)</code>
	 * <br>
	 * <br><b>Description:</b>
	 * <br>Tells the delegate that the specified row is now selected.
	 * <br>
	 * <br>The delegate handles cell selections in this method. One of the things it can do is assign the check-mark image.
	 * <br>
	 * <br>This delegate method is not called if the TableView's selection mode is <code>TableViewSelectionModeNoSelection</code>.
	 * 
	 * @param indexPath
	 * @param tableView
	 */
	
	public void didSelectRowAtIndexPathForTableView (IndexPath indexPath,  TableView tableView)
	{
		String methodName = "didSelectRowAtIndexPathForTableView";
		Object _arrParameters[] = { indexPath,  tableView};
		
		if(this.respondsToMethodWithName(methodName)){
			 performMethodNamedWithParameters(methodName, _arrParameters);
		}
	}

	/**
	 * <br><b>Contingency:</b>
	 * <br><i>Optional Delegate Method</i>
	 * <br>
	 * <br><b>Delegate Method Called:</b>
	 * <br><code>public void willDeselectRowAtIndexPathForTableView (IndexPath indexPath,  TableView tableView)</code>
	 * <br>
	 * <br><b>Description:</b>
	 * <br>Tells the delegate that a specified row is about to be deselected.
	 * <br>
	 * <br>This method is only called if there is an existing selection when the user tries to select a different row. The delegate is sent this method for the previously selected row. 
	 * <br>
	 * <br>You can use <code>TableViewCellSelectionStyle.None</code> to disable the appearance of the cell highlight on touch-down.
	 * <br>
	 * <br>This delegate method is not called if the TableView's selection mode is <code>TableViewSelectionModeNoSelection</code>.
	 * @param indexPath
	 * @param tableView
	 */
	
	public void willDeselectRowAtIndexPathForTableView (IndexPath indexPath,  TableView tableView)
	{
		String methodName = "willDeselectRowAtIndexPathForTableView";
		Object _arrParameters[] = { indexPath,  tableView};
		
		if(this.respondsToMethodWithName(methodName)){
			 performMethodNamedWithParameters(methodName, _arrParameters);
		}
	}

	/**
	 * <br><b>Contingency:</b>
	 * <br><i>Optional Delegate Method</i>
	 * <br>
	 * <br><b>Delegate Method Called:</b>
	 * <br><code>didDeselectRowAtIndexPathForTableView (IndexPath indexPath,  TableView tableView)</code>
	 * <br>
	 * <br><b>Description:</b>
	 * <br>Tells the delegate that the specified row is now deselected.
	 * <br>
	 * <br>The delegate handles row deselections in this method. It could, for example, remove the check-mark image associated with the row.
	 * <br>
	 * <br>This delegate method is not called if the TableView's selection mode is <code>TableViewSelectionModeNoSelection</code>.
	 * @param indexPath
	 * @param tableView
	 */
	
	public void didDeselectRowAtIndexPathForTableView (IndexPath indexPath,  TableView tableView)
	{
		String methodName = "didDeselectRowAtIndexPathForTableView";
		Object _arrParameters[] = { indexPath,  tableView};
		
		if(this.respondsToMethodWithName(methodName)){
			 performMethodNamedWithParameters(methodName, _arrParameters);
		}
	}
	
	/**
	 * @category Modifying the Header and Footer of Sections
	 */
	
	/**
	 * <br><b>Contingency:</b>
	 * <br><i>Optional Delegate Method</i>: Will default to empty header
	 * <br>
	 * <br><b>Delegate Method Called:</b>
	 * <br><code>public View headerForSectionWithReusableViewForTableView (Integer section, View reusableView, TableView tableView)</code>
	 * <br>
	 * <br><b>Description:</b> 
	 * <br>Queries DataSource delegate for a TableViewHeader. Returning a <code>null</code> view will remove the header from the current section. 
	 * <br>
	 * @param section
	 * @param reusableView
	 * @param tableView
	 * @return View of subclass TableViewHeader
	 */
	
	public View viewForHeaderInSectionWithReusableViewForTableView (Integer section, View reusableView, TableView tableView)
	{
		String methodName = "headerForSectionWithReusableViewForTableView";
		Object _arrParameters[] = {section, reusableView, tableView};
		
		if(this.respondsToMethodWithName(methodName)){
			return (View) performMethodNamedWithParameters(methodName, _arrParameters);
		}
		return null;
	}
	
	
	/**
	 * <br><b>Contingency:</b>
	 * <br><i>Optional Delegate Method</i>: Will default to empty footer
	 * <br>
	 * <br><b>Delegate Method Called:</b>
	 * <br><code>public View footerForSectionWithReusableViewForTableView (Integer section, View reusableView, TableView tableView)</code>
	 * <br>
	 * <br><b>Description:</b> 
	 * <br>Queries DataSource delegate for a footer view. Returning a <code>null</code> view will remove the footer from the current section. 
	 * <br>
	 * @param section
	 * @param reusableView
	 * @param tableView
	 * @return View of subclass TableViewFooter
	 */
	
	
	public View viewForFooterInSectionWithReusableViewForTableView (Integer section, View reusableView, TableView tableView)
	{
		String methodName = "footerForSectionWithReusableViewForTableView";
		Object _arrParameters[] = {section, reusableView, tableView};
		
		if(this.respondsToMethodWithName(methodName)){
			return (View) performMethodNamedWithParameters(methodName, _arrParameters);
		}
		return null;
	}
	
	/**
	 * <br><b>Contingency:</b>
	 * <br><i>Optional Delegate Method</i>:
	 * <br>
	 * <br><b>Delegate Method Called:</b>
	 * <br><code>public void willDisplayHeaderViewForSectionForTableView (View view, Integer section, TableView tableView)</code>
	 * <br>
	 * <br><b>Description:</b> 
	 * <br> Tells the delegate that a header view is about to be displayed for the specified section.
	 * @param view
	 * @param section
	 * @param tableView
	 */
	
	public void willDisplayHeaderViewForSectionForTableView (View view, Integer section, TableView tableView)
	{
		String methodName = "willDisplayHeaderViewForSectionForTableView";
		Object _arrParameters[] = {view, section, tableView};
		
		if(this.respondsToMethodWithName(methodName)){
			 performMethodNamedWithParameters(methodName, _arrParameters);
		}
	}
	
	/**
	 * <br><b>Contingency:</b>
	 * <br><i>Optional Delegate Method</i>:
	 * <br>
	 * <br><b>Delegate Method Called:</b>
	 * <br><code>public void willDisplayFooterViewForSectionForTableView (View view, Integer section, TableView tableView)</code>
	 * <br>
	 * <br><b>Description:</b> 
	 * <br> Tells the delegate that a footer view is about to be displayed for the specified section.
	 * @param view
	 * @param section
	 * @param tableView
	 */
	
	public void willDisplayFooterViewForSectionForTableView (View view, Integer section, TableView tableView)
	{
		String methodName = "willDisplayFooterViewForSectionForTableView";
		Object _arrParameters[] = { view, section, tableView};
		
		if(this.respondsToMethodWithName(methodName)){
			performMethodNamedWithParameters(methodName, _arrParameters);
		}
	}
	
	/**
	 * Selection/Highlighting/Adding_and_Removing
	 */
	
	
	
	
	
}
	
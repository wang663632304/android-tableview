package co.bytemark.android.opentools.tableview;

/**
 * <br>IndexPath enables you to get the represented row index (m_nRow property) and section index (m_nSection property), and to construct an index path from a given row index and section index. 
 * <br>
 * <br>Because rows are located within their sections, you usually must evaluate the section index number before you can identify the row by its index number.
 * <br>
 * <br>The IndexPath class also stores the number of rows in its section and the number of sections in the table to allow quick calculation of view properties of the TableViewStyleGrouped style. These values should not be used for calculating TableView data. 
 */
public class IndexPath 
{
	public final int m_nSection;
	public final int m_nRow;
	
	private final int m_nRowsInSection;
	private  final int m_nSectionInTable;
	

	public IndexPath(int _nSection, int _nRow, int _nRowsInSection, int _nSectionInTable) 
	{
		this.m_nSection = _nSection;
		this.m_nRow = _nRow;
		this.m_nRowsInSection = _nRowsInSection;
		this.m_nSectionInTable = _nSectionInTable;
	}

	public int getSection() 
	{
		return m_nSection;
	}

	public int getRow() 
	{
		return m_nRow;
	}
	
	public int getRowsInSection()
	{
		return m_nRowsInSection;
	}

	public boolean isHeader() 
	{
		return m_nRow == -1;
	}
	
	public boolean isFooter() 
	{
		return m_nRow == m_nRowsInSection;
	}

	public boolean isFirstSection() 
	{
		return m_nSection == 0;
	}


	public boolean isFirstCellOfGroup() 
	{
		return (m_nRow == 0);
	}
	
	public boolean isLastCellInSection ()
	{
		return (m_nRow == (m_nRowsInSection - 1));
	}
	
	public boolean isLastCellInTable ()
	{
		if (isInLastSection()){
			return (m_nRow == (m_nRowsInSection - 1));
		}
		return false;
	}
	
	public boolean isInLastSection ()
	{
		return (m_nSection == (m_nSectionInTable - 1));
	}


	public boolean isEqual(IndexPath indexPath) 
	{
		boolean isEqual = true;
		if (indexPath == null){
			System.out.println("WARNING: IP is null");
			return false;
		}
		
		if(this.m_nRow != indexPath.m_nRow){
			isEqual = false; 
		}else if (this.m_nSection != indexPath.m_nSection){
			isEqual = false;
		}

		System.out.println("WIN: Same "+this.m_nRow+":"+this.m_nSection);
		return isEqual;
	}

	@Override
	public String toString() 
	{
		return "IndexPath [section=" + m_nSection + ", row=" + m_nRow + " RowSections="+m_nRowsInSection+" Sections="+m_nSectionInTable+"]";
	}
}